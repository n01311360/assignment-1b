﻿//Add a class to your visual studio project by doing the following
//right click project > add > new item > c# > class

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1
{
    public class Client
    {
        private string _fname;
        private string _lname;
        private string _email;

        //property accessors.
        public string Fname {
            get { return _fname; }
            set { _fname = value; }
        }
        public string Lname {
            get { return _lname; }
            set { _lname = value; }
        }
        public string Email {
            get { return _email; }
            set { _email = value; }
        }
    }
}