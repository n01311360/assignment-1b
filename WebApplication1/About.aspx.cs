﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class About : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void reserve(object sender, EventArgs e)
        {
            //This is where you get your items for your objects.

            string clientfname = fname.Text;
            string clientlname = lname.Text;
            string email = clientEmail.Text;

            Client newclient = new Client();
            newclient.Fname = clientfname;
            newclient.Lname = clientlname;
            newclient.Email = email;

            int numGuests = int.Parse(guestAmount.SelectedValue);
            Party newparty = new Party(numGuests);
            //Just assuming the RVSP is for today
            DateTime today = DateTime.Today;

            Rvsp newrvsp = new Rvsp(newclient, newparty, today);

            //When you bind all of the fields for each class,
            //use the printreceipt method.

            result.InnerHtml = newrvsp.PrintReceipt();




        }
    }
}