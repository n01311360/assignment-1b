﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1
{
    public class Rvsp
    {
        //client, party are the class names
        //use rvsp_ prefix to denote a variable in the 
        //rvsp object.

        public Client rvsp_client;
        public Party rvsp_party;
        DateTime rvsp_time;

        public Rvsp(Client c, Party p, DateTime time)
        {
            rvsp_client = c;
            rvsp_party = p;
            rvsp_time = time;
        }
        public string PrintReceipt()
        {
            /*
            Here is where you put your code for the receipt.
            This is yours:
            
            string confirmation = "Your Reservation Confirmation:<br>";
            confirmation += "Your revservation time is :" + CalculateOrder().ToString() + "<br/>";
            confirmation += "Name:" + clientFName + clientLname + "<br/>";
            confirmation += "Email:" + clientEmail + "<br/>";
            confirmation += "Reservation time:" + resTimes + "<br/>";
            confirmation += "Guest Amount:" + string.Join(" ", guestAmount.ToArray()) + "<br/>"
            */

            string receipt = "";
            receipt += "Your name is " + rvsp_client.Fname;
            receipt += "Your party amount is " + rvsp_party.guestAmount.ToString();
            receipt += "Your amount is " + CalcOrder();
            return receipt;

        }
        public float CalcOrder()
        {
            float total = 0f;
            //use pieces of information from your class to calculate the total
            total += 20.99f * rvsp_party.guestAmount;
            return total;

        }
        





    }
}