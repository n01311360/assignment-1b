﻿<%@ Page Title="Charlie's Dinner" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Confirm.cs" Inherits="WebApplication1.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%:Title %>.</h2>
  <h2>WELCOME</h2>
    <h3>Book your reservation now</h3>

 First Name: <asp:TextBox runat="server" id="fname"></asp:TextBox>
<asp:RequiredFieldValidator runat="server" id="fnamevalidator" ControlToValidate="fname" errorMessage="**Please enter a First name"> </asp:RequiredFieldValidator>
 <br>
Last Name: <asp:Textbox runat="server" Id="lname"></asp:Textbox>
<asp:RequiredFieldValidator runat="server" id="lnameValidator1" ControlToValidate="fname" errorMessage="**Please enter a Last name"> </asp:RequiredFieldValidator>
   
    <br>
Email: <asp:Textbox runat="server" Id="clientEmail"></asp:Textbox>
<asp:RegularExpressionValidator runat="server" ID="emailValidator" ControlToValidate="clientEmail" validationgroup="clientEmail" ErrorMessage="**Please Provide an Email"></asp:RegularExpressionValidator>
  <br />        
Select a time<asp:DropDownList ID="resTimes" runat="server">
    <asp:ListItem Enabled="true" Text=""></asp:ListItem>
     <asp:ListItem Text="8-11:Breakfast" Value="1"></asp:ListItem>
     <asp:ListItem Text="10-1:Brunch" Value="2"></asp:ListItem>
     <asp:ListItem Text="12-4:Lunch" Value="4"></asp:ListItem>
     <asp:ListItem Text="5-11:Dinner" Value="5"></asp:ListItem>
 </asp:DropDownList>
    <asp:RequiredFieldValidator runat="server" ID="timesvalidator" ControlToValidate="resTimes" SetFocusOnError="true" ErrorMessage="You must select a time"></asp:RequiredFieldValidator>
    <br />
    <br/>

    
Number of Guest<asp:DropDownList Id="guestAmount" runat="server" RepeatDirection="Horizontal">
    <asp:ListItem Text="1-3" Value="3"></asp:ListItem>
    <asp:listItem Text="4-6" Value="6"></asp:listItem>
    <asp:listItem Text="6-8" Value="7"></asp:listItem>
    <asp:listItem Text="10+" Value ="10"></asp:listItem>
    </asp:DropDownList>
    <br /> 
    <p>Would you like to signup for special offers?</p>
    <asp:RadioButtonList ID="Elist" runat="server" repeatlayout="Flow">
        <asp:ListItem>Email me special offers</asp:ListItem>
        <asp:ListItem>Not Today</asp:ListItem>
    </asp:RadioButtonList>
    <asp:RequiredFieldValidator runat="server" ID="Elistvalidator" ControlToValidate="Elist" ErrorMessage="**please select"></asp:RequiredFieldValidator>

    <asp:Button id="Submitbtn" text="submit" runat="server" OnClick="reserve" />
       
    <asp:ValidationSummary ID="ValidationSummary" runat="server" 
   DisplayMode = "BulletList" ShowSummary = "true" HeaderText="Errors:" />

   <div runat="server" id="result"></div>

    <br />

</asp:Content>
